---
title: "The Operationalisation of Self-Identity in Reasoned Action Models: Explained Variance"
author: "Marwin H. M. Snippe, Gjalt-Jorn Y. Peters & Gerjo Kok"
date: "`r format(Sys.time(), '%H:%M:%S on %Y-%m-%d %Z (GMT%z)')`"
output:
  html_document:
    toc: true
    toc_float:
      collapsed: false
      smooth_scroll: true
      toc_depth: 4
    code_folding: hide
    df_print: paged
editor_options: 
  chunk_output_type: console
---

```{r knitr-setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE);
knitr::opts_chunk$set(rows.print = 30);
```

```{r packages-and-paths}

###-----------------------------------------------------------------------------
### Quick activation or deactivation of specific analysis chunks
###-----------------------------------------------------------------------------

EVALUATE_SCALE_DIAGNOSTICS <- TRUE;
EVALUATE_INFLUENTIAL_CASES <- TRUE;

###-----------------------------------------------------------------------------
### Load packages
###-----------------------------------------------------------------------------

### Check for presence of packages:
### - userfriendlyscience
### - ufs
### - psych
### - here
### - car
### - careless

###-----------------------------------------------------------------------------
### Get most recent versions from GitLab
###-----------------------------------------------------------------------------

tryCatch(remotes::install_gitlab('r-packages/ufs',
                                 error=invisible, quiet = TRUE,
                                 dependencies=FALSE, upgrade=FALSE),
         error=invisible);

###-----------------------------------------------------------------------------
### Paths and files
###-----------------------------------------------------------------------------

repoPath <- here::here();
workingPath <- here::here("results-intermediate-output");
dataPath <- here::here("results-data");

### Regular expressions that match the private and public datafile names
privateDataFileRegEx <- '\\[PRIVATE]';
publicDataFileRegEx <- '\\[PUBLIC]';

```


```{r scale-definitions}

behaviors <-
  c("alcohol", "condoms", "exercise");

###-----------------------------------------------------------------------------
### Scale definitions
###-----------------------------------------------------------------------------

scales <- list();

scales$selfIdentity <- c('Self-identity:\n' = 'Selfidentity_kindofperson',
                         'Self-identity:\n' = 'Selfidentity_seemyselfas', 
                         'Self-identity:\n' = 'Selfidentity_importantpart',
                         'Self-identity:\n' = 'Selfidentity_behaviormeansmoretantheactself');
itemTexts <-
  list(selfIdentity = stats::setNames(names(scales$selfIdentity),
                                      nm = scales$selfIdentity));

scales$attitude <- c('Attitude_bad_good', 'Attitude_unpleasant_pleasant',
                     'Attitude_harmful_beneficial', 'Attitude_boring_interesting');

scales$importance <- c('Importancescale_unimportant_important',
                       'Importancescale_notessential_essential',
                       'Importancescale_notsignificant_significant');


scales$perceivedNorms <- c('Injunctivenorm_importantpeople',
                           'Injunctivenorm_mostpeopleapprove',
                           'Descriptivenorm_closefriends',
                           'Descriptivenorm_peoplelikeme');

scales$pbc <- c('Perceivedcontrol_forme',
                'Perceivedcontrol_reallywantto',
                'Perceivedcontrol_confident');

scales$intention <- c('Intention_intend',
                      'Intention2willing',
                      'Intention3expect');

scales$pastBehavior <- c('Past_haveused', 'Past_howoften');


fullScales <- scales;

### Specify measurement models
measurementModelSpecs <- list();
for (currentScale in names(scales)) {
  measurementModelSpecs[[currentScale]] <- paste0(currentScale, ' =~ ',
                                                  paste0(scales[[currentScale]],
                                                         collapse=" + "));
}

### 
nonSIvars <- scales[names(scales) != 'selfIdentity'];

### Generate abbreviated variable names
abbr <- abbreviate(names(scales));

```

# Loading and preparing data

## Data loading {.tabset}

### Overview

Please click "Details" to see the code and output for this step in the analysis procedure.

### Details

```{r load-processed-data}

###-----------------------------------------------------------------------------
### Load processed datafiles (resulting from the Conceptual Independence script)
###-----------------------------------------------------------------------------

dat <- list();

for (currentDataset in list.files(dataPath)) {
  ### Store new datafile
  dataSetName <- gsub("_processed\\.csv",
                      "",
                      currentDataset);
  dat[[dataSetName]] <-
    read.csv(file.path(dataPath,
                       currentDataset));
}

```

# Descriptives

## Scale diagnostics {.tabset .tabset-pills}

These are extensive scale diagnostics, presented separately for each dataset and within each dataset, separately for each scale. Use the tabs to navigate.

```{r scale-diagnostics, results='asis', eval=EVALUATE_SCALE_DIAGNOSTICS}

###-----------------------------------------------------------------------------
### Order scale diagnostics
###-----------------------------------------------------------------------------

scaleDiagnosisObjects <- list();
for (currentDataset in names(dat)) {
  scaleDiagnosisObjects[[currentDataset]] <- list();
  pander::pandoc.header(paste0(currentDataset, " {.tabset .tabset-pills}"), 3);
  for (currentScale in names(scales)) {
    if (length(scales[[currentScale]]) > 1) {
      scaleDiagnosisObjects[[currentDataset]][[currentScale]] <-
        ufs::scaleDiagnosis(
            data = dat[[currentDataset]],
            items = scales[[currentScale]],
            headingLevel = 5,
            poly = FALSE
          );
      pander::pandoc.header(paste0(currentScale, " {.tabset .tabset-pills}"), 4);
      cat(
        knitr::knit_print(
          scaleDiagnosisObjects[[currentDataset]][[currentScale]]
        )
      );
    }
  }
}

```

## Correlation matrices {.tabset .tabset-pills}

```{r correlations, results='asis'}

###-----------------------------------------------------------------------------
### Order scale diagnostics
###-----------------------------------------------------------------------------

for (currentDataset in names(dat)) {
  pander::pandoc.header(paste0(currentDataset, " {.tabset .tabset-pills}"), 3);
  cat(pander::pander(
    ufs::associationMatrix(dat[[currentDataset]][, names(scales)])
  ));
}

```

# Analyses

## Regression analyses {.tabset .tabset-pills}

```{r regression-analyses, results='asis'}
### Removed results="asis" so that we don't have to fix the markup = now

for (currentDataset in names(dat)) {
  
  ufs::cat0("\n\n### ", currentDataset, " {.tabset .tabset-pills}\n\n");

  pander::pandoc.header("Attitude, Norms, PBC", 4);

  print(knitr::knit_print(
    rosetta::regr(intention ~ attitude + pbc + perceivedNorms,
                  data=dat[[currentDataset]],
                  headingLevel = 4)
  ));
  
  pander::pandoc.header("Attitude, Norms, PBC, Past Behavior", 4);

  print(knitr::knit_print(
    rosetta::regr(intention ~ attitude + perceivedNorms + pbc + pastBehavior,
                  data=dat[[currentDataset]],
                  headingLevel = 5)
  ));
  
  pander::pandoc.header("Attitude, Importance, Norms, PBC, Past Behavior", 4);

  print(knitr::knit_print(
    rosetta::regr(intention ~ attitude+ importance + perceivedNorms + pbc + pastBehavior,
                  data=dat[[currentDataset]],
                  headingLevel = 5)
  ));
  
  pander::pandoc.header("Attitude, Norms, PBC, Self-Identity, Past Behavior", 4);

    print(knitr::knit_print(
      rosetta::regr(intention ~ attitude + perceivedNorms + pbc + selfIdentity + pastBehavior,
                    data=dat[[currentDataset]],
                    headingLevel = 5)
    ));
  
  pander::pandoc.header("Attitude, importance, Norms, PBC, Self-Identity, Past Behavior", 4);

  print(knitr::knit_print(
    rosetta::regr(intention ~ attitude + perceivedNorms + pbc + selfIdentity  + importance + pastBehavior,
                  data=dat[[currentDataset]],
                  headingLevel = 5)
  ));
  
  pander::pandoc.header("Attitude, Norms, PBC, Self-Identity", 4);

  print(knitr::knit_print(
    rosetta::regr(intention ~ attitude + perceivedNorms + pbc + selfIdentity,
                  data=dat[[currentDataset]],
                  headingLevel = 5)
  ));
  
  
  pander::pandoc.header("Attitude, Norms, PBC, Importance, Past Behavior", 4);

  print(knitr::knit_print(
    rosetta::regr(intention ~ attitude + perceivedNorms + pbc + importance + pastBehavior,
                  data=dat[[currentDataset]],
                  headingLevel = 5)
  ));

}

```







<!---------------------------------------------------------------------------->
<!-- Relocating table of contents and tweaking some styles
<!---------------------------------------------------------------------------->

<script>
// Move TOC to the Table of Contents heading (with id "contents")
$(function() {
  $( "#TOC" ).insertAfter( $( "#contents" ) );
});
</script>

<style>
.svg-figure {
  width: 100%;
}
</style>

